# Dev

## Prerequisites: 
- docker 
- docker compose
- make


## Run
```sh
make build
make up
```
Go to: http://localhost:3001/ to see the site!


## Close:
```sh
make down
```
## Images
![alt text](./images/test-1.png "Test image for app")
![alt text](./images/test-2.png "Test image for modal")