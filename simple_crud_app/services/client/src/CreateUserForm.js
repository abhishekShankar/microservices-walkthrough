import React, {useState} from 'react';
import PropTypes from 'prop-types';

CreateUser.propTypes = {
  createUser: PropTypes.func,
};

function CreateUser({createUser}) {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  return (
    <div className="pt-5">
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Username</th>
            <th>Email</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <input
                type="text"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </td>
            <td>
              <input
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </td>
            <td>
              <button
                type="button"
                className="btn btn-success"
                onClick={() => {
                  createUser(username, email);
                  setUsername('');
                  setEmail('');
                }}
              >
                Add User
              </button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default CreateUser;
