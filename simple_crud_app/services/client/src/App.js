import axios from 'axios';
import React, {useEffect, useState} from 'react';

import CreateUserForm from './CreateUserForm';
import UserModal from './UserModal';
import UserTable from './UserTable';

function App() {
  const [users, setUsers] = useState([]);
  const [show, setShow] = useState(false);
  const [user, setUser] = useState({});

  const fetchUsers = () => {
    axios
        .get(`http://localhost:8001/api/v1/users/`)
        .then((res) => {
          setUsers(res.data);
        })
        .catch((err) => {
          console.error(err);
        });
  };

  const updateUser = (id, username, email) => {
    if (!id) {
      console.error('Id is not valid: ', id);
    }
    const data = {};
    if (username) {
      data.username = username;
    }
    if (email) {
      data.email = email;
    }
    axios
        .patch(`http://localhost:8001/api/v1/users/${id}/`, data)
        .then((res) => {
          fetchUsers();
        })
        .catch((err) => {
          console.error(err);
        });
  };

  const createUser = (username, email) => {
    username = username.trim();
    email = email.trim();
    const data = {username, email};

    axios
        .post(`http://localhost:8001/api/v1/users/`, data)
        .then((res) => {
          fetchUsers();
        })
        .catch((err) => {
          console.error(err);
        });
  };

  const deleteUser = (id) => {
    if (!id) {
      console.error('Id is not valid: ', id);
      return;
    }
    axios
        .delete(`http://localhost:8001/api/v1/users/${id}/`)
        .then((res) => {
          fetchUsers();
        })
        .catch((err) => {
          console.error(err);
        });
  };

  useEffect(() => {
    fetchUsers();
  }, []);

  return (
    <div className="container pt-5">
      <UserModal
        key={user.id || 'id'}
        show={show}
        setShow={setShow}
        user={user}
        handleUpdate={updateUser}
      />
      <CreateUserForm
        createUser={createUser}
      />
      <UserTable
        users={users}
        setModalVisibility={setShow}
        setCurrentUser={setUser}
        deleteUser={deleteUser}
      />
    </div>
  );
}

export default App;
