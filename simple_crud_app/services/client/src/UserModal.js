import React, {useState} from 'react';
import PropTypes from 'prop-types';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';


UserModal.propTypes = {
  show: PropTypes.bool,
  setShow: PropTypes.func,
  user: PropTypes.arrayOf(PropTypes.object),
  handleUpdate: PropTypes.func,
};

function UserModal({show, setShow, user, handleUpdate}) {
  const [username, setUsername] = useState(user.username || '?');
  const [email, setEmail] = useState(user.email || '?');
  const handleClose = () => {
    setShow(false);
    setUsername(user.username || '?');
    setEmail(user.email || '?');
  };
  return (
    <>
      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title as="h4">
            {username} + {email}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input
            type="text"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <input
            type="text"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              if (user.username !== username || user.email !== email) {
                handleUpdate(user.id, username, email);
              }
              handleClose();
            }}
          >
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}


export default UserModal;
