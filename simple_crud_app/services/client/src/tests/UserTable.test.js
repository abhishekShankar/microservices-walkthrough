import React from 'react';
import {shallow} from 'enzyme';

import UserTable from '../UserTable';

const createUser = (id, username, email) => {
  return {id, username, email};
};

const users = [
  createUser(1, 'one', 'one@gmail.com'),
  createUser(2, 'two', 'two@gmail.com'),
];

test('UserTable renders properly', () => {
  const wrapper = shallow(<UserTable users={users} />);

  const tbody = wrapper.find('tbody');
  const rows = tbody.find('tr');
  const firstRowColumns = rows
      .at(0)
      .find('td')
      .map((column) => {
        return column.text();
      });

  expect(rows.length).toBe(2);
  expect(firstRowColumns[0]).toBe('1');
  expect(firstRowColumns[1]).toBe('one');
  expect(firstRowColumns[2]).toBe('one@gmail.com');

  const secondRowColumns = rows
      .at(1)
      .find('td')
      .map((column) => {
        return column.text();
      });

  expect(secondRowColumns[0]).toBe('2');
  expect(secondRowColumns[1]).toBe('two');
  expect(secondRowColumns[2]).toBe('two@gmail.com');
});
