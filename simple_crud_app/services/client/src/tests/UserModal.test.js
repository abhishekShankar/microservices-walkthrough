import React from 'react';
import {mount} from 'enzyme';
import UserModal from '../UserModal';

const createUser = (id, username, email) => {
  return {id, username, email};
};

const user = createUser(1, 'one', 'one@gmail.com');

test('UserModal renders properly', () => {
  const wrapper = mount(
      <UserModal
        user={user}
        show={true}
        setShow={() => {}}
        handleUpdate={() => {}}
      />,
  );

  const inputs = wrapper.find('input');
  const buttons = wrapper.find('Button');
  const closeButton = buttons.at(0);
  const saveChangesButton = buttons.at(1);

  expect(wrapper.find('.modal-title').length).toEqual(1);
  expect(wrapper.find('.modal-title').text()).toEqual(
      user.username + ' + ' + user.email,
  );

  expect(inputs.length).toBe(2);
  expect(inputs.at(0).props().value).toBe('one');
  expect(inputs.at(1).props().value).toBe('one@gmail.com');


  expect(buttons.length).toBe(2);
  expect(closeButton.text()).toBe('Close');
  expect(saveChangesButton.text()).toBe('Save Changes');
});
