import React from 'react';
import {shallow} from 'enzyme';

import CreateUserForm from '../CreateUserForm';

test('CreateUserForm renders properly', () => {
  const wrapper = shallow(<CreateUserForm createUser={() => {}} />);

  const inputs = wrapper.find('input');
  const buttons = wrapper.find('button');
  const addUserButton = buttons.at(0);

  expect(inputs.length).toBe(2);
  expect(buttons.length).toBe(1);
  expect(addUserButton.text()).toBe('Add User');
});
