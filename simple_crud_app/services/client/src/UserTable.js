import React from 'react';
import PropTypes from 'prop-types';

UserTable.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object),
  setModalVisibility: PropTypes.func,
  setCurrentUser: PropTypes.func,
  deleteUser: PropTypes.func,
};


function UserTable({users, setModalVisibility, setCurrentUser, deleteUser}) {
  const getUsersForTable = () => {
    return users.map((user) => {
      const id = user.id || 'unknown';
      const username = user.username || 'unknown';
      const email = user.email || 'unknown';
      return (
        <tr key={id}>
          <td>{id}</td>
          <td>{username}</td>
          <td>{email}</td>
          <td>
            <button
              type="button"
              className="btn btn-info"
              onClick={() => {
                setModalVisibility(true);
                setCurrentUser(user);
              }}
            >
              Edit
            </button>
            <button
              type="button"
              className="ml-3 btn btn-danger"
              onClick={() => {
                deleteUser(id);
              }}
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Username</th>
          <th scope="col">Email</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>{getUsersForTable()}</tbody>
    </table>
  );
}

export default UserTable;
