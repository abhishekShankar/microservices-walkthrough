from django.http import HttpResponse
from rest_framework import viewsets

from .models import User
from .serializers import UserSerializer


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
