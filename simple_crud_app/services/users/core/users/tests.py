import json

from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status

from .models import User
from .serializers import UserSerializer

"""
python ./core/manage.py test users
"""

# initialize the APIClient app
client = Client()


USER_LIST = "users-list"
USER_DETAIL = "users-detail"
CONTENT_TYPE = "application/json"


class TestGetSingleUser(TestCase):
    def setUp(self):
        self.one = User.objects.create(username="one", email="one@gmail.com")

    def test_get_valid_single_user(self):
        response = client.get(reverse(USER_DETAIL, kwargs={"pk": self.one.pk}))
        user = User.objects.get(pk=self.one.pk)
        serializer = UserSerializer(user)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_user(self):
        response = client.get(reverse(USER_DETAIL, kwargs={"pk": 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class TestGetAllUser(TestCase):
    url = reverse(USER_LIST)

    def setUp(self):
        self.one = User.objects.create(username="one", email="one@gmail.com")
        self.two = User.objects.create(username="two", email="two@gmail.com")

    def test_get_all_users(self):
        response = client.get(self.url)
        all_users = User.objects.all()
        serializer = UserSerializer(all_users, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestCreateUser(TestCase):
    url = reverse(USER_LIST)

    def setUp(self):
        self.one = User.objects.create(username="one", email="one@gmail.com")
        self.valid_payload = {"username": "user1", "email": "user1@gmail.com"}
        self.invalid_payload = {}

    def test_create_user(self):
        response = client.post(
            self.url, data=json.dumps(self.valid_payload), content_type=CONTENT_TYPE
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_user(self):
        payloads = [
            self.invalid_payload,
            {"username": "user1",},
            {"email": "user1@gmail.com",},
        ]

        for payload in payloads:
            response = client.post(
                self.url, data=json.dumps(payload), content_type=CONTENT_TYPE
            )

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_duplicate_user(self):
        response = client.post(
            self.url,
            data=json.dumps({"username": "one", "email": "one@gmail.com",}),
            content_type=CONTENT_TYPE,
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestUpdateUser(TestCase):
    def setUp(self):
        self.one = User.objects.create(username="one", email="one@gmail.com")
        self.valid_payload = {"username": "Muffy2", "email": "new_email@gmail.com"}

    def test_valid_update_user(self):
        response = client.put(
            reverse(USER_DETAIL, kwargs={"pk": self.one.pk}),
            data=json.dumps(self.valid_payload),
            content_type=CONTENT_TYPE,
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_update_user(self):
        payloads = [{"username": "Muffy2"}, {"email": "two@gmail.com"}, {}]
        for payload in payloads:
            response = client.put(
                reverse(USER_DETAIL, kwargs={"pk": self.one.pk}),
                data=json.dumps(payload),
                content_type=CONTENT_TYPE,
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestPartialUpdateUser(TestCase):
    def setUp(self):
        self.one = User.objects.create(username="one", email="one@gmail.com")

    def test_valid_partial_update_user(self):
        payloads = [{"username": "Muffy2"}, {"email": "two@gmail.com"}]
        for payload in payloads:
            response = client.patch(
                reverse(USER_DETAIL, kwargs={"pk": self.one.pk}),
                data=json.dumps(payload),
                content_type=CONTENT_TYPE,
            )

            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_partial_update_user(self):
        payloads = [{"username": ""}, {"email": ""}]
        for payload in payloads:
            response = client.patch(
                reverse(USER_DETAIL, kwargs={"pk": self.one.pk}),
                data=json.dumps(payload),
                content_type=CONTENT_TYPE,
            )

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestDeleteUser(TestCase):
    def setUp(self):
        self.one = User.objects.create(username="one", email="one@gmail.com")

    def test_valid_delete_user(self):
        response = client.delete(reverse(USER_DETAIL, kwargs={"pk": self.one.pk}))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_user(self):
        response = client.delete(reverse(USER_DETAIL, kwargs={"pk": 30}))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
