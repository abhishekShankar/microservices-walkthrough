from django.urls import include, path, re_path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register("users", views.UserViewSet, basename="users")

urlpatterns = [
    path("index", views.index, name="index"),
    re_path("", include(router.urls)),
]
