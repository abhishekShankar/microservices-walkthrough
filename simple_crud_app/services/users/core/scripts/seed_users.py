from users.models import User


def run():
    number_of_users = User.objects.count()
    if number_of_users > 0:
        print("Users already exists!")
        return
    User.objects.create(username="user_one", email="user_one@gmail.com")
    User.objects.create(username="user_two", email="user_two@gmail.com")
