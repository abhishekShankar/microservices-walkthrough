# Main


# Apps 

## Simple Crud App 
This app was made to try out CRUD operations in react. We created a table editor that took a username and email where we could: 
- Create users 
- Read users from database
- Update users username or email
- Remove users 

We have also done some tests on it. More info can be found on our [Simple Crud App readme].

## Instagram Clone (Work in progress)
- Trying to replicate instagram's frontend

[Simple Crud App readme]: <https://gitlab.com/abhishekShankar/microservices-walkthrough/-/tree/master/simple_crud_app/README.md>
