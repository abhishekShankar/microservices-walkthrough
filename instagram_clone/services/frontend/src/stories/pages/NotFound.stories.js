import React from 'react';

import NotFound from '../../pages/NotFound';

export default {
  title: 'Pages/NotFound',
  component: NotFound,
};

const Template = () => <NotFound />;

export const Initial = Template.bind({});
