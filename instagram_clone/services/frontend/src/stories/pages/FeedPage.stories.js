import React from 'react';
import FeedPage from '../../pages/FeedPage';

export default {
  title: 'Pages/FeedPage',
  component: FeedPage,
};

const Template = (args) => <FeedPage {...args} />;

export const Initial = Template.bind({});

export const Loading = Template.bind({});
Loading.args = { loading: true };
