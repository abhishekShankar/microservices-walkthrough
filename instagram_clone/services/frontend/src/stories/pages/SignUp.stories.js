import React from 'react';
import SignUp from '../../pages/SignUp';

export default {
  title: 'Pages/SignUp',
  component: SignUp,
};

const Template = () => <SignUp />;

export const Initial = Template.bind({});
