import React from 'react';

import Login from '../../pages/Login';

export default {
  title: 'Pages/Login',
  component: Login,
};

const Template = () => <Login />;

export const Initial = Template.bind({});
