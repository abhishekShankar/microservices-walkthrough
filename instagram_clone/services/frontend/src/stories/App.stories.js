import React from 'react';

import App from '../App';

export default {
  title: 'App/App',
  component: App,
};

const Template = (args) => <App {...args} />;

export const InitialApp = Template.bind({});
InitialApp.args = {};
