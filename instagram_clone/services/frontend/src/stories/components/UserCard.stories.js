import React from 'react';

import UserCard from '../../components/shared/UserCard';
import defaultUser from '../../utils/defaultUser';

export default {
  title: 'Components/UserCard',
  component: UserCard,
};

const Template = (args) => <UserCard {...args} />;

export const DefaultState = Template.bind({});
DefaultState.args = { user: defaultUser() };
