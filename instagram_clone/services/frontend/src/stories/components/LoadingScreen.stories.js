import React from 'react';

import LoadingScreen from '../../components/shared/LoadingScreen';

export default {
  title: 'Components/Shared/LoadingScreen',
  component: LoadingScreen,
};

const Template = (args) => <LoadingScreen {...args} />;
export const DefaultState = Template.bind({});
DefaultState.args = {};
