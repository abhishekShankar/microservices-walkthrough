import React from 'react';

import FeedPostSkeleton from '../../components/feed/FeedPostSkeleton';

export default {
  title: 'Components/Feed/FeedPostSkeleton',
  component: FeedPostSkeleton,
};

const Template = () => <FeedPostSkeleton />;
export const DefaultState = Template.bind({});
