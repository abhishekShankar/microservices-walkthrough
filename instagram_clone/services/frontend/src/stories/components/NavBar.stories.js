import React from 'react';

import NavBar from '../../components/shared/NavBar';
import { defaultCurrentUser, defaultNotifications } from '../../data';

export default {
  title: 'Components/NavBar',
  component: NavBar,
};

const Template = (args) => <NavBar {...args} />;

export const LoggedIn = Template.bind({});
LoggedIn.args = {
  currentUser: defaultCurrentUser,
  notifications: defaultNotifications,
};

export const LoggedOut = Template.bind({});
LoggedOut.args = {};
