import React from 'react';

import FeedPost from '../../components/feed/FeedPost';
import defaultPost from '../../utils/defaultPost';

export default {
  title: 'Components/Feed/FeedPost',
  component: FeedPost,
};

const post = defaultPost();
const Template = (args) => <FeedPost {...args} />;
export const DefaultState = Template.bind({});
DefaultState.args = { id: post.id, post };
