import { v4 as uuid } from 'uuid';
import defaultUser from './defaultUser';
import defaultComment from './defaultComment';

const defaultPost = () => ({
  id: uuid(),
  likes: 10,
  caption: `I'm not ready for the final episode of @jujutsukaisen this Friday 💀 #JujutsuKaisen #Anime #Crunchyroll #ItadoriYuji #YujiItadori #GojoSatoru #SatoruGojo #NobaraKugisaki #KugisakiNobara #MegumiFushiguro #FushiguroMegumi #Sukuna #SukunaRyomen #RyomenSukuna #JJK #manga #MAPPA #Panda #aoitodo`,
  user: defaultUser(),
  media:
    'https://trello-attachments.s3.amazonaws.com/60623600ac895c7615155233/60623615a03d8052801404e3/7984c85ceb74908441a8ae5a0fbe197f/image.png',
  comments: [
    defaultComment('Comment from user'),
    defaultComment('comment from user 2'),
  ],
  created_at: '2020-02-28T03:08:14.522421+00:00',
});

export default defaultPost;
