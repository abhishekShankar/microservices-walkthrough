import { v4 as uuid } from 'uuid';

const defaultUser = () => ({
  id: uuid(),
  username: `username: ${uuid().substr(0, 3)}`,
  name: 'name',
  profile_image:
    'https://lh3.googleusercontent.com/ogw/ADGmqu89YxvFgMryJDSMw4L8eJgl5D2zQz8PDYUNgTd9=s192-c-mo',
});

export default defaultUser;
