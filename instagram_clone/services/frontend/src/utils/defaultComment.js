import { v4 as uuid } from 'uuid';
import defaultUser from './defaultUser';

const defaultComment = (defaultContent = 'default') => ({
  id: uuid(),
  user: defaultUser(),
  content: defaultContent,
});

export default defaultComment;
