import React from 'react';
import {
  Card,
  CardHeader,
  TextField,
  Button,
  Typography,
  makeStyles,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import FacebookIconBlue from '../images/facebook-icon-blue.svg';
import FacebookIconWhite from '../images/facebook-icon-white.png';
import IconSheet from '../images/icon-sheet.png';
import SEO from '../components/shared/Seo';

/* Login page: /pages/login.js */
export const useLoginPageStyles = makeStyles({
  signUpCard: {
    maxWidth: 348,
    padding: '16px 40px',
    marginBottom: 10,
    display: 'grid',
    alignItems: 'center',
    gridTemplateColumns: '2fr 1fr',
  },
  card: {
    maxWidth: 348,
    padding: '16px 40px',
    marginBottom: 10,
  },
  section: {
    display: 'grid',
    placeItems: 'center',
    height: '100vh',
  },
  cardHeader: {
    backgroundImage: `url(${IconSheet})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: '-98px 0',
    height: 20,
    width: 148,
    margin: '22px auto 12px',
  },
  textField: {
    marginBottom: 6,
  },
  button: {
    margin: '8px 0px',
  },
  typography: {
    margin: '10px 0px',
  },
  orContainer: {
    margin: '10px 0px',
    display: 'grid',
    gridTemplateColumns: '1fr auto 1fr',
    gridGap: 18,
    placeItems: 'center',
  },
  orLine: {
    justifySelf: 'stretch',
    height: 1,
    background: '#e6e6e6',
  },
  facebookIcon: {
    height: 16,
    width: 16,
    marginRight: 8,
  },
  signUpButton: {
    textTransform: 'normal',
  },
});

function LoginPage() {
  const classes = useLoginPageStyles();

  return (
    <>
      <SEO title="Log In" />
      <section className={classes.section}>
        <article>
          <Card className={classes.card}>
            <CardHeader className={classes.cardHeader} />
            <form>
              <TextField
                fullWidth
                variant="filled"
                label="Username"
                margin="dense"
                className={classes.textField}
                autoComplete="username"
              />
              <TextField
                fullWidth
                variant="filled"
                label="Password"
                type="password"
                margin="dense"
                className={classes.textField}
                autoComplete="current-password"
              />
              <Button
                variant="contained"
                fullWidth
                color="primary"
                className={classes.button}
                type="submit"
              >
                Log In
              </Button>
            </form>
            <div className={classes.orContainer}>
              <div className={classes.orLine} />
              <div>
                <Typography variant="body2" color="textSecondary">
                  OR
                </Typography>
              </div>
              <div className={classes.orLine} />
            </div>
            <LoginWithFacebook color="primary" iconColor="blue" />
            <Button fullWidth color="primary">
              <Typography variant="caption">Forgot password?</Typography>
            </Button>
          </Card>
          <Card className={classes.signUpCard}>
            <Typography align="right" variant="body2">
              Don't have an account?
            </Typography>
            <Link to="/accounts/emailsignup">
              <Button color="primary" className={classes.signUpButton}>
                Sign up
              </Button>
            </Link>
          </Card>
        </article>
      </section>
    </>
  );
}

export function LoginWithFacebook({ color, iconColor }) {
  const classes = useLoginPageStyles();
  const facebookIcon =
    iconColor === 'blue' ? FacebookIconBlue : FacebookIconWhite;

  return (
    <Button fullWidth color={color}>
      <img
        src={facebookIcon}
        alt="facebook icon"
        className={classes.facebookIcon}
      />
      Log in with Facebook
    </Button>
  );
}

LoginWithFacebook.propTypes = {
  color: PropTypes.string.isRequired,
  iconColor: PropTypes.string.isRequired,
};

export default LoginPage;
