import React from 'react';
import {
  Card,
  TextField,
  Button,
  Typography,
  makeStyles,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import IconSheet from '../images/icon-sheet.png';
import { LoginWithFacebook } from './Login';
import SEO from '../components/shared/Seo';

/* SignUp page: /pages/signup.js */

export const useSignUpPageStyles = makeStyles({
  card: { maxWidth: 348, padding: '16px 40px', marginBottom: 10 },
  section: {
    display: 'grid',
    placeItems: 'center',
    height: '100vh',
  },
  cardHeader: {
    backgroundImage: `url(${IconSheet})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: '-98px 0',
    height: 51,
    width: 175,
    margin: '22px auto 12px',
  },
  cardHeaderSubHeader: {
    textAlign: 'center',
    fontWeight: 'bold !important',
    lineHeight: 1.2,
    color: '#999',
    margin: '0 0 20px',
  },
  textField: {
    marginBottom: 6,
  },
  button: {
    margin: '10px 0px 16px 0px',
  },
  orContainer: {
    margin: '10px 0px',
    display: 'grid',
    gridTemplateColumns: '1fr auto 1fr',
    gridGap: 18,
    placeItems: 'center',
  },
  orLine: {
    justifySelf: 'stretch',
    height: 1,
    background: '#e6e6e6',
  },
  loginCard: {
    maxWidth: 348,
    padding: '16px 40px',
    marginBottom: 10,
    display: 'grid',
    alignItems: 'center',
    gridTemplateColumns: '3fr 2fr',
  },
  loginButton: {
    justifySelf: 'start',
    tectTransform: 'normal',
  },
});

function SignUpPage() {
  const classes = useSignUpPageStyles();

  return (
    <>
      <SEO title="Sign Up" />
      <section className={classes.section}>
        <article>
          <Card className={classes.card}>
            <div className={classes.cardHeader} />
            <Typography className="classes.crdHeaderSubHeader">
              Sign up to see photos and videos from your friends.
            </Typography>
            <LoginWithFacebook
              color="primary"
              iconColor="white"
              variant="contained"
            />
            <div className={classes.orContainer}>
              <div className={classes.orLine} />
              <div>
                <Typography variant="body2" color="textSecondary">
                  OR
                </Typography>
              </div>
              <div className={classes.orLine} />
            </div>
            <form>
              <TextField
                fullWidth
                variant="filled"
                label="Email"
                type="email"
                margin="dense"
                className={classes.textField}
              />
              <TextField
                fullWidth
                variant="filled"
                label="Full Name"
                margin="dense"
                className={classes.textField}
              />
              <TextField
                fullWidth
                variant="filled"
                label="Username"
                margin="dense"
                className={classes.textField}
                autoComplete="username"
              />
              <TextField
                fullWidth
                variant="filled"
                label="Password"
                type="password"
                margin="dense"
                className={classes.textField}
                autoComplete="current-password"
              />
              <Button
                variant="contained"
                fullWidth
                color="primary"
                className={classes.button}
                type="submit"
              >
                Sign Up
              </Button>
            </form>
          </Card>
          <Card className={classes.loginCard}>
            <Typography align="right" variant="body2">
              Have an account?
            </Typography>
            <Link to="/accounts/login">
              <Button color="primary" className={classes.loginButton}>
                Log In
              </Button>
            </Link>
          </Card>
        </article>
      </section>
    </>
  );
}

export default SignUpPage;
