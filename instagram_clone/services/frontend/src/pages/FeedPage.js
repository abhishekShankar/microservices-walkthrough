import React, { useState } from 'react';
import { Hidden, makeStyles, Paper, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';

import Layout from '../components/shared/Layout';
import UserCard from '../components/shared/UserCard';
import defaultPost from '../utils/defaultPost';
import defaultUser from '../utils/defaultUser';
import FollowButton from '../components/shared/FollowButton';
import LoadingScreen from '../components/shared/LoadingScreen';
import FeedPostSkeleton from '../components/feed/FeedPostSkeleton';

const FeedPost = React.lazy(() => import('../components/feed/FeedPost'));

const useLoadingLargeStyles = makeStyles({
  container: {
    height: 50,
    width: 32,
    margin: '0 auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  loading: {
    animation: '$spinner-spin12 1.2s steps(12) infinite',
  },
  '@keyframes spinner-spin12': {
    '0%': {
      transform: 'rotate(0deg)',
    },
    to: {
      transform: 'rotate(360deg)',
    },
  },
});

export const LoadingLargeIcon = (props) => {
  const classes = useLoadingLargeStyles();

  return (
    <div className={classes.container} {...props}>
      <svg
        aria-label="Loading..."
        className={classes.loading}
        viewBox="0 0 100 100"
      >
        <rect
          fill="#555555"
          height={6}
          opacity={0}
          rx={3}
          ry={3}
          transform="rotate(-90 50 50)"
          width={25}
          x={72}
          y={47}
        />
        <rect
          fill="#555555"
          height={6}
          opacity="0.08333333333333333"
          rx={3}
          ry={3}
          transform="rotate(-60 50 50)"
          width={25}
          x={72}
          y={47}
        />
        <rect
          fill="#555555"
          height={6}
          opacity="0.16666666666666666"
          rx={3}
          ry={3}
          transform="rotate(-30 50 50)"
          width={25}
          x={72}
          y={47}
        />
        <rect
          fill="#555555"
          height={6}
          opacity="0.25"
          rx={3}
          ry={3}
          transform="rotate(0 50 50)"
          width={25}
          x={72}
          y={47}
        />
        <rect
          fill="#555555"
          height={6}
          opacity="0.3333333333333333"
          rx={3}
          ry={3}
          transform="rotate(30 50 50)"
          width={25}
          x={72}
          y={47}
        />
        <rect
          fill="#555555"
          height={6}
          opacity="0.4166666666666667"
          rx={3}
          ry={3}
          transform="rotate(60 50 50)"
          width={25}
          x={72}
          y={47}
        />
        <rect
          fill="#555555"
          height={6}
          opacity="0.5"
          rx={3}
          ry={3}
          transform="rotate(90 50 50)"
          width={25}
          x={72}
          y={47}
        />
        <rect
          fill="#555555"
          height={6}
          opacity="0.5833333333333334"
          rx={3}
          ry={3}
          transform="rotate(120 50 50)"
          width={25}
          x={72}
          y={47}
        />
        <rect
          fill="#555555"
          height={6}
          opacity="0.6666666666666666"
          rx={3}
          ry={3}
          transform="rotate(150 50 50)"
          width={25}
          x={72}
          y={47}
        />
        <rect
          fill="#555555"
          height={6}
          opacity="0.75"
          rx={3}
          ry={3}
          transform="rotate(180 50 50)"
          width={25}
          x={72}
          y={47}
        />
        <rect
          fill="#555555"
          height={6}
          opacity="0.8333333333333334"
          rx={3}
          ry={3}
          transform="rotate(210 50 50)"
          width={25}
          x={72}
          y={47}
        />
        <rect
          fill="#555555"
          height={6}
          opacity="0.9166666666666666"
          rx={3}
          ry={3}
          transform="rotate(240 50 50)"
          width={25}
          x={72}
          y={47}
        />
      </svg>
    </div>
  );
};

export const useFeedPageStyles = makeStyles((theme) => ({
  container: {
    display: 'grid',
    gridTemplateColumns: 'minmax(auto, 600px) 300px',
    gridGap: 35,
    [theme.breakpoints.down('sm')]: {
      gridTemplateColumns: 'minmax(auto, 600px)',
      justifyContent: 'center',
    },
    '&.slickSlider': {
      display: 'grid',
    },
  },
  sidebarContainer: {
    display: 'grid',
    margin: '0px 28px 24px',
    justifyContent: 'center',
    gridTemplateColumns: 'minmax(auto, 300px)',
  },
  sidebarWrapper: { position: 'fixed', width: 293 },
}));

function FeedPage({ loading }) {
  if (loading) {
    return <LoadingScreen />;
  }

  const classes = useFeedPageStyles();
  const [isEndOfFeed] = useState(false);

  const getPosts = () => {
    const arr = new Array(5).fill(1);
    return arr.map(() => {
      const post = defaultPost();
      return (
        <React.Suspense key={post.id} fallback={<FeedPostSkeleton />}>
          <FeedPost id={post.id} post={post} />
        </React.Suspense>
      );
    });
  };

  return (
    <Layout title="Feed page" marginTop={120}>
      <div className={classes.container}>
        {/* Feed post */}
        <div>{getPosts()}</div>
        {/* Sidebar */}
        <Hidden smDown>
          <div className={classes.sidebarContainer}>
            <div className={classes.sidebarWrapper}>
              <UserCard avatarSize={50} user={defaultUser()} />
              <FeedSideSuggestions />
            </div>
          </div>
        </Hidden>
        {!isEndOfFeed && <LoadingLargeIcon />}
      </div>
    </Layout>
  );
}

export const useFeedSideSuggestionsStyles = makeStyles((theme) => ({
  article: {
    margin: '12px 0',
    gridTemplateColumns: 'minmax(auto, 600px)',
    justifyContent: 'center',
  },
  card: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridTemplateColumns: 'minmax(auto, 500px)',
    gridGap: 10,
    alignItems: 'center',
    padding: '8px 16px !important',
  },
  typography: {
    paddingLeft: `16px !important`,
    [theme.breakpoints.down('xs')]: {
      fontSize: '1rem !important',
    },
  },
  paper: {
    padding: '8px 0 !important',
  },
}));

const useLoadingStyles = makeStyles({
  container: {
    height: 18,
    width: 18,
    margin: '0 auto',
  },
  loadingSvg: {
    animation: '$IGCoreSpinnerSpin8 .8s steps(8) infinite',
  },
  '@keyframes IGCoreSpinnerSpin8': {
    '0%': {
      transform: 'rotate(180deg)',
    },
    to: {
      transform: 'rotate(540deg)',
    },
  },
});

export const LoadingIcon = () => {
  const classes = useLoadingStyles();

  return (
    <div>
      <div className={classes.container}>
        <svg
          aria-label="Loading..."
          className={classes.loadingSvg}
          viewBox="0 0 100 100"
        >
          <rect
            fill="#555555"
            height={10}
            opacity={0}
            rx={5}
            ry={5}
            transform="rotate(-90 50 50)"
            width={28}
            x={67}
            y={45}
          />
          <rect
            fill="#555555"
            height={10}
            opacity="0.125"
            rx={5}
            ry={5}
            transform="rotate(-45 50 50)"
            width={28}
            x={67}
            y={45}
          />
          <rect
            fill="#555555"
            height={10}
            opacity="0.25"
            rx={5}
            ry={5}
            transform="rotate(0 50 50)"
            width={28}
            x={67}
            y={45}
          />
          <rect
            fill="#555555"
            height={10}
            opacity="0.375"
            rx={5}
            ry={5}
            transform="rotate(45 50 50)"
            width={28}
            x={67}
            y={45}
          />
          <rect
            fill="#555555"
            height={10}
            opacity="0.5"
            rx={5}
            ry={5}
            transform="rotate(90 50 50)"
            width={28}
            x={67}
            y={45}
          />
          <rect
            fill="#555555"
            height={10}
            opacity="0.625"
            rx={5}
            ry={5}
            transform="rotate(135 50 50)"
            width={28}
            x={67}
            y={45}
          />
          <rect
            fill="#555555"
            height={10}
            opacity="0.75"
            rx={5}
            ry={5}
            transform="rotate(180 50 50)"
            width={28}
            x={67}
            y={45}
          />
          <rect
            fill="#555555"
            height={10}
            opacity="0.875"
            rx={5}
            ry={5}
            transform="rotate(225 50 50)"
            width={28}
            x={67}
            y={45}
          />
        </svg>
      </div>
    </div>
  );
};

function FeedSideSuggestions({ loading }) {
  const classes = useFeedSideSuggestionsStyles();

  const getUserSuggestions = () => {
    if (loading) {
      return <LoadingIcon />;
    }

    return Array.from({ length: 5 }, () => defaultUser()).map((user) => {
      return (
        <div key={user.id} className={classes.card}>
          <UserCard user={user} />
          <FollowButton />
        </div>
      );
    });
  };

  return (
    <article className={classes.article}>
      <Paper className={classes.paper}>
        <Typography
          color="textSecondary"
          variant="subtitle2"
          component="h2"
          align="left"
          gutterBottom
          className={classes.typography}
        >
          Suggestions For You
        </Typography>
        {getUserSuggestions()}
      </Paper>
    </article>
  );
}

FeedSideSuggestions.propTypes = { loading: PropTypes.bool };
FeedSideSuggestions.defaultProps = { loading: false };

FeedPage.propTypes = { loading: PropTypes.bool };
FeedPage.defaultProps = { loading: false };

export default FeedPage;
