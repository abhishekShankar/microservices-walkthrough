import React, { useState } from 'react';
import {
  Button,
  Divider,
  Hidden,
  Link,
  makeStyles,
  TextField,
  Typography,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import LinesEllipsis from 'react-lines-ellipsis';

import UserCard from '../shared/UserCard';

/* FeedPost component: /components/feed/FeedPost.js */
export const useFeedPostStyles = makeStyles((theme) => ({
  article: {
    border: '1px solid #e6e6e6',
    background: '#ffffff',
    marginBottom: 60,
    [theme.breakpoints.down('xs')]: {
      border: 'unset',
      marginBottom: 0,
    },
  },
  postHeader: {
    borderBottom: '1px solid rgba(var(--ce3,239,239,239),1)',
    display: 'grid',
    gridAutoFlow: 'column',
    gridTemplateColumns: 'auto minmax(auto, 20px)',
    gridGap: 10,
    alignItems: 'center',
    padding: 16,
  },
  moreIcon: {
    height: 24,
    width: 18,
    justifySelf: 'center',
    '&:hover': {
      cursor: 'pointer',
    },
  },
  image: {
    width: '100%',
  },
  postButtons: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridTemplateColumns: '24px 24px 24px minmax(24px, auto)',
    gridGap: 16,
    padding: '6px 0px !important',
  },
  postButtonsWrapper: {
    padding: '0px 16px 8px !important',
  },
  commentUsername: {
    fontWeight: '600 !important',
  },
  datePosted: {
    fontSize: '10px !important',
  },
  likes: {
    fontWeight: '600 !important',
    '&:hover': {
      cursor: 'pointer',
    },
  },
  like: {
    animation: '$like-button-animation 0.45s',
    animationTimingFunction: 'ease-in-out',
    transform: 'scale(1)',
  },
  liked: {
    animation: '$liked-button-animation 0.45s',
    animationTimingFunction: 'ease-in-out',
    transform: 'scale(1)',
  },
  '@keyframes like-button-animation': {
    '0%': { transform: 'scale(1)' },
    '25%': { transform: 'scale(1.2)' },
    '50%': { transform: 'scale(0.95)' },
    '100%': { transform: 'scale(1)' },
  },
  '@keyframes liked-button-animation': {
    '0%': { transform: 'scale(1)' },
    '25%': { transform: 'scale(1.2)' },
    '50%': { transform: 'scale(0.95)' },
    '100%': { transform: 'scale(1)' },
  },
  textField: {
    padding: '10px 0px !important',
  },
  root: {
    fontSize: '14px !important',
  },
  underline: {
    '&::before': {
      border: 'none !important',
    },
    '&::after': {
      border: 'none !important',
    },
    '&:hover&:before': {
      border: 'none !important',
    },
  },
  commentContainer: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridTemplateColumns: 'auto minmax(auto, 56px)',
    padding: '0px 0px 0px 16px !important',
  },
  commentButton: {
    width: '48px !important',
    padding: 'unset',
  },
  moreButton: {
    color: '#999 !important',
    padding: '0px !important',
    '&:hover': {
      background: 'transparent !important',
    },
  },
  saveIcon: {
    justifySelf: 'right',
  },
  commentsLink: {
    color: '#999',
    margin: '5px 0 !important',
  },
  collapsed: {
    display: 'flex',
    alignItems: 'center',
  },
  expanded: {
    display: 'block',
  },
  caption: {
    fontFamily: `-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", Helvetica, Arial, sans-serif`,
    fontSize: '14px !important',
  },
  captionWrapper: {
    display: 'flex',
    alignItems: 'center',
    wordBreak: 'break-all',
  },
  username: {
    fontWeight: '600 !important',
    marginRight: '5px !important',
  },
}));

export const MoreIcon = (props) => (
  <svg
    aria-label="More options"
    className="_8-yf5"
    fill="#262626"
    height={16}
    viewBox="0 0 48 48"
    width={16}
    {...props}
  >
    <circle clipRule="evenodd" cx={8} cy={24} fillRule="evenodd" r={4.5} />
    <circle clipRule="evenodd" cx={24} cy={24} fillRule="evenodd" r={4.5} />
    <circle clipRule="evenodd" cx={40} cy={24} fillRule="evenodd" r={4.5} />
  </svg>
);

function FeedPost({ id, post }) {
  const classes = useFeedPostStyles();
  const [showCaption, setShowCaption] = useState(false);
  const { media, likes, user, caption, comments } = post;
  const setCaptionToTrue = () => {
    setShowCaption(true);
  };
  return (
    <>
      <article className={classes.article}>
        <div className={classes.postHeader}>
          <UserCard user={user} />
          <MoreIcon className={classes.moreIcon} />
        </div>
        {/* Post image */}
        <div>
          <img src={media} alt="Post Media" className={classes.image} />
        </div>
        {/* Feed Post Buttons */}
        <div className={classes.postButtonsWrapper}>
          <div className={classes.postButtons}>
            <LikeButton />
            <Link to={`{/p/${id}}`}>
              <CommentIcon />
            </Link>
            <ShareIcon />
            <SaveButton />
          </div>
          <Typography className={classes.like}>
            <span>{likes === 1 ? '1 Like' : `${likes} likes`}</span>
          </Typography>
          <div className={showCaption ? classes.expanded : classes.collapsed}>
            <Link to={`/${user.username}`}>
              <Typography
                variant="subtitle2"
                component="span"
                className={classes.username}
              >
                {user.username}
              </Typography>
            </Link>
            {showCaption ? (
              <Typography variant="body2" component="span">
                {caption}
              </Typography>
            ) : (
              <div className={classes.captionWrapper}>
                <LinesEllipsis
                  text={caption}
                  className={classes.caption}
                  maxLine="0"
                  ellipsis="..."
                  basedOn="letters"
                />
                <Button
                  className={classes.moreButton}
                  onClick={setCaptionToTrue}
                >
                  more
                </Button>
              </div>
            )}
          </div>
          <Link to={`/p/${id}`}>
            <Typography
              className={classes.commentsLink}
              variant="body2"
              components="div"
            >
              View all {comments.length} comments
            </Typography>
          </Link>
          {comments.map((comment) => {
            return (
              <div key={comment.id}>
                <Link to={`/${comment.user.username}`}>
                  <Typography
                    variant="subtitle2"
                    component="span"
                    className={classes.commentUsername}
                  >
                    {comment.user.username}
                  </Typography>{' '}
                  <Typography variant="body2" component="span">
                    {comment.content}
                  </Typography>
                </Link>
              </div>
            );
          })}
          <Typography color="textSecondary" className={classes.datePosted}>
            2 DAYS AGO
          </Typography>
        </div>
        <Hidden xsDown>
          <Divider />
          <Comment />
        </Hidden>
      </article>
    </>
  );
}

export const LikeIcon = (props) => (
  <svg
    aria-label="Like"
    className="_8-yf5"
    fill="#262626"
    height={24}
    viewBox="0 0 48 48"
    width={24}
    {...props}
  >
    <path
      clipRule="evenodd"
      d="M34.3 3.5C27.2 3.5 24 8.8 24 8.8s-3.2-5.3-10.3-5.3C6.4 3.5.5 9.9.5 17.8s6.1 12.4 12.2 17.8c9.2 8.2 9.8 8.9 11.3 8.9s2.1-.7 11.3-8.9c6.2-5.5 12.2-10 12.2-17.8 0-7.9-5.9-14.3-13.2-14.3zm-1 29.8c-5.4 4.8-8.3 7.5-9.3 8.1-1-.7-4.6-3.9-9.3-8.1-5.5-4.9-11.2-9-11.2-15.6 0-6.2 4.6-11.3 10.2-11.3 4.1 0 6.3 2 7.9 4.2 3.6 5.1 1.2 5.1 4.8 0 1.6-2.2 3.8-4.2 7.9-4.2 5.6 0 10.2 5.1 10.2 11.3 0 6.7-5.7 10.8-11.2 15.6z"
      fillRule="evenodd"
    />
  </svg>
);

export const RemoveIcon = (props) => (
  <svg
    aria-label="Remove"
    className="_8-yf5"
    fill="#262626"
    height={24}
    viewBox="0 0 48 48"
    width={24}
    {...props}
  >
    <path d="M43.5 48c-.4 0-.8-.2-1.1-.4L24 28.9 5.6 47.6c-.4.4-1.1.6-1.6.3-.6-.2-1-.8-1-1.4v-45C3 .7 3.7 0 4.5 0h39c.8 0 1.5.7 1.5 1.5v45c0 .6-.4 1.2-.9 1.4-.2.1-.4.1-.6.1z" />
  </svg>
);

export const UnlikeIcon = (props) => (
  <svg
    aria-label="Unlike"
    className="_8-yf5"
    fill="#ed4956"
    height={24}
    viewBox="0 0 48 48"
    width={24}
    {...props}
  >
    <path
      clipRule="evenodd"
      d="M35.3 35.6c-9.2 8.2-9.8 8.9-11.3 8.9s-2.1-.7-11.3-8.9C6.5 30.1.5 25.6.5 17.8.5 9.9 6.4 3.5 13.7 3.5 20.8 3.5 24 8.8 24 8.8s3.2-5.3 10.3-5.3c7.3 0 13.2 6.4 13.2 14.3 0 7.8-6.1 12.3-12.2 17.8z"
      fillRule="evenodd"
    />
  </svg>
);

const LikeButton = () => {
  const classes = useFeedPostStyles();
  const [liked, setLiked] = useState(false);
  const Icon = liked ? UnlikeIcon : LikeIcon;
  const className = liked ? classes.liked : classes.like;

  const handleLike = () => {
    setLiked(true);
  };
  const handleUnlike = () => {
    setLiked(false);
  };
  const onClick = liked ? handleUnlike : handleLike;

  return <Icon className={className} onClick={onClick} />;
};

export const CommentIcon = (props) => (
  <svg
    aria-label="Comment"
    className="_8-yf5"
    fill="#262626"
    height={24}
    viewBox="0 0 48 48"
    width={24}
    {...props}
  >
    <path
      clipRule="evenodd"
      d="M47.5 46.1l-2.8-11c1.8-3.3 2.8-7.1 2.8-11.1C47.5 11 37 .5 24 .5S.5 11 .5 24 11 47.5 24 47.5c4 0 7.8-1 11.1-2.8l11 2.8c.8.2 1.6-.6 1.4-1.4zm-3-22.1c0 4-1 7-2.6 10-.2.4-.3.9-.2 1.4l2.1 8.4-8.3-2.1c-.5-.1-1-.1-1.4.2-1.8 1-5.2 2.6-10 2.6-11.4 0-20.6-9.2-20.6-20.5S12.7 3.5 24 3.5 44.5 12.7 44.5 24z"
      fillRule="evenodd"
    />
  </svg>
);

export const ShareIcon = (props) => (
  <svg
    aria-label="Share Post"
    className="_8-yf5"
    fill="#262626"
    height={24}
    viewBox="0 0 48 48"
    width={24}
    {...props}
  >
    <path d="M46.5 3.5h-45C.6 3.5.2 4.6.8 5.2l16 15.8 5.5 22.8c.2.9 1.4 1 1.8.3L47.4 5c.4-.7-.1-1.5-.9-1.5zm-40.1 3h33.5L19.1 18c-.4.2-.9.1-1.2-.2L6.4 6.5zm17.7 31.8l-4-16.6c-.1-.4.1-.9.5-1.1L41.5 9 24.1 38.3zM14.7 48.4l2.9-.7" />
  </svg>
);

export const SaveIcon = (props) => (
  <svg
    aria-label="Save"
    className="_8-yf5"
    fill="#262626"
    height={24}
    viewBox="0 0 48 48"
    width={24}
    {...props}
  >
    <path d="M43.5 48c-.4 0-.8-.2-1.1-.4L24 29 5.6 47.6c-.4.4-1.1.6-1.6.3-.6-.2-1-.8-1-1.4v-45C3 .7 3.7 0 4.5 0h39c.8 0 1.5.7 1.5 1.5v45c0 .6-.4 1.2-.9 1.4-.2.1-.4.1-.6.1zM24 26c.8 0 1.6.3 2.2.9l15.8 16V3H6v39.9l15.8-16c.6-.6 1.4-.9 2.2-.9z" />
  </svg>
);

const SaveButton = () => {
  const classes = useFeedPostStyles();
  const [saved, setSaved] = useState(false);
  const Icon = saved ? RemoveIcon : SaveIcon;

  const handleSaved = () => {
    setSaved(true);
  };
  const handleRemove = () => {
    setSaved(false);
  };
  const onClick = saved ? handleRemove : handleSaved;

  return <Icon className={classes.saveIcon} onClick={onClick} />;
};
const Comment = () => {
  const classes = useFeedPostStyles();
  const [content, setContent] = useState('');
  const handleChangeContent = ({ target: { value } }) => {
    setContent(value);
  };
  return (
    <div className={classes.commentContainer}>
      <TextField
        fullWidth
        value={content}
        placeholder="Add a comment..."
        multiline
        rowsMax={2}
        rows={1}
        onChange={handleChangeContent}
        className={classes.textField}
        InputProps={{
          classes: {
            root: classes.root,
            underline: classes.underline,
          },
        }}
      />
      <Button
        color="primary"
        className={classes.commentButton}
        disabled={!content.trim()}
      >
        Post
      </Button>
    </div>
  );
};

FeedPost.propTypes = {
  id: PropTypes.string.isRequired,
  post: PropTypes.oneOfType([PropTypes.object]).isRequired,
};

export default FeedPost;
