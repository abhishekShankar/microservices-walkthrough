import React from 'react';
import { Avatar, makeStyles, Typography } from '@material-ui/core';

import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export const useUserCardStyles = makeStyles({
  avatar: {
    width: ({ avatarSize = 44 }) => avatarSize,
    height: ({ avatarSize = 44 }) => avatarSize,
  },
  typography: {
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    color: '#262626',
  },
  wrapper: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridTemplateColumns: 'min-content auto',
    gridGap: 12,
    alignItems: 'center',
    width: '100%',
  },
  nameWrapper: {
    overflow: 'hidden',
    whiteSpace: 'nowrap',
  },
  link: {
    textDecoration: 'none',
  },
});

function UserCard({ user, avatarSize }) {
  const classes = useUserCardStyles({ avatarSize });
  const { username, name, profile_image: profileImage } = user;

  return (
    <div className={classes.wrapper}>
      <Link to={`/${username}`}>
        <Avatar
          src={profileImage}
          alt="User Avatar"
          className={classes.avatar}
        />
      </Link>
      <div className={classes.nameWrapper}>
        <Link className={classes.link} to={`/${username}`}>
          <Typography variant="subtitle2" className={classes.typography}>
            {username}
          </Typography>
        </Link>
        <Typography
          color="textSecondary"
          variant="body2"
          className={classes.typography}
        >
          {name}
        </Typography>
      </div>
    </div>
  );
}

UserCard.propTypes = {
  user: PropTypes.oneOfType([PropTypes.object]).isRequired,
  avatarSize: PropTypes.number,
};

UserCard.defaultProps = {
  avatarSize: 44,
};

export default UserCard;
