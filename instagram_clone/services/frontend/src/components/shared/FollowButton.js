import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, makeStyles } from '@material-ui/core';

export const useFollowButtonStyles = makeStyles({
  button: {
    height: '30px !important',
    width: '75px !important',
    padding: '0px 16px !important',
    marginTop: ({ side }) => `${side ? '0px !important' : '10px !important'}`,
    color: '#3897f0',
    backgroundColor: 'transparent',
    '&.MuiButton-containedPrimary:hover': {
      backgroundColor: '#fafafa',
      boxShadow: 'none',
    },
  },
});

const FollowButton = ({ side }) => {
  const classes = useFollowButtonStyles({ side });
  const [isFollowing, setFollowing] = useState(false);

  const handleFollowing = () => {
    setFollowing(true);
  };

  const handleNotFollowing = () => {
    setFollowing(false);
  };

  const followButton = (
    <Button
      variant={side ? 'text' : 'contained'}
      color="primary"
      className={classes.button}
      onClick={handleFollowing}
      fullWidth
    >
      Follow
    </Button>
  );

  const followingButton = (
    <Button
      variant={side ? 'text' : 'outlined'}
      className={classes.button}
      onClick={handleNotFollowing}
      fullWidth
    >
      Following
    </Button>
  );

  return isFollowing ? followingButton : followButton;
};

FollowButton.propTypes = {
  side: PropTypes.bool,
};

FollowButton.defaultProps = {
  side: false,
};

export default FollowButton;
