import React from 'react';
import PropTypes from 'prop-types';
import { Person } from '@material-ui/icons';
import { userProfilePictureStyles } from '../../styles';

function ProfilePicture({ image, isOwner, size }) {
  const classes = userProfilePictureStyles({ size, isOwner });

  return (
    <section className={classes.section}>
      {image ? (
        <div className={classes.wrapper}>
          <img src={image} alt="user profile" className={classes.image} />
        </div>
      ) : (
        <div className={classes.wrapper}>
          <Person className={classes.person} />
        </div>
      )}
    </section>
  );
}

ProfilePicture.propTypes = {
  image: PropTypes.string,
  isOwner: PropTypes.bool,
  // eslint-disable-next-line react/require-default-props
  size: PropTypes.string,
};

ProfilePicture.defaultProps = {
  image:
    'https://lh3.googleusercontent.com/ogw/ADGmqu89YxvFgMryJDSMw4L8eJgl5D2zQz8PDYUNgTd9=s192-c-mo',
  isOwner: true,
};

export default ProfilePicture;
