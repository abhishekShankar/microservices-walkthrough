import React from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';

function SEO({ title }) {
  const titleText = title ? `${title} • Instagram` : 'Instagram';
  return (
    <Helmet>
      <title>{titleText}</title>
    </Helmet>
  );
}

SEO.propTypes = {
  title: PropTypes.string.isRequired,
};

export default SEO;
