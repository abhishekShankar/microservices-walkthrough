import React from 'react';
import { Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useGridPostStyles } from '../../styles';
/* eslint-disable jsx-a11y/click-events-have-key-events */

function GridPost({ post }) {
  const history = useHistory();
  const classes = useGridPostStyles();

  const handleOpenPostModal = () => {
    history.push({
      pathname: `/p/${post.id}`,
      state: { modal: true },
    });
  };

  return (
    // eslint-disable-next-line jsx-a11y/click-events-have-key-events
    // eslint-disable-next-line jsx-a11y/no-static-element-interactions
    <div className={classes.gridPostContainer} onClick={handleOpenPostModal}>
      <div className={classes.gridPostOverlay}>
        <div className={classes.gridPostInfo}>
          <span className={classes.likes} />
          <Typography>{post.likes}</Typography>
        </div>
        <div className={classes.gridPostInfo}>
          <span className={classes.comments} />
          <Typography>{post.comments.length}</Typography>
        </div>
      </div>
      <img src={post.media} alt="Post cover" className={classes.image} />
    </div>
  );
}

export default GridPost;
