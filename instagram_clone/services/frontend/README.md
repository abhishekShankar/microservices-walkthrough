
# Make commands

- start: start app
- lint: show linting errors
- format: format code
- install.dev: install all dependencies
- storybook: start storybook

# Storybook
To see the components working, you need `npm` and run the command below.
```bash
npx http-server ./storybook-static
```